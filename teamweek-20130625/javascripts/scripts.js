(function() {

    var matched, browser;

    // Use of jQuery.browser is frowned upon.
    // More details: http://api.jquery.com/jQuery.browser
    // jQuery.uaMatch maintained for back-compat
    jQuery.uaMatch = function( ua ) {
	    ua = ua.toLowerCase();

	    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		    /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		    /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		    /(msie) ([\w.]+)/.exec( ua ) ||
		    ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
		    [];

	    return {
		    browser: match[ 1 ] || "",
		    version: match[ 2 ] || "0"
	    };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
	    browser[ matched.browser ] = true;
	    browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
	    browser.webkit = true;
    } else if ( browser.webkit ) {
	    browser.safari = true;
    }

    jQuery.browser = browser;

})();

$(function() {
    $('.accordion-toggler').click(function(e) {
        e.preventDefault();
        var $accordion = $(e.target).closest('.accordion');
        $accordion.toggleClass('accordion-closed');
    });
    
    $('.signin').click($.proxy(function(e) {
        e.preventDefault();
        
        if (!this.signinOverlay) { 
            this.signinOverlay = new Overlay();
        }
        this.signinOverlay.show();
        this.signinOverlay.$modal.on('onClose', $.proxy(function() {
            this.signinOverlay.$modal.off('onClose');
            $('.sign-in-popup').hide();
        }, this));
        
        $('.sign-in-popup').show();
    }, this));
    
    
    $('.first-content .text, .second-content .text').jbLoader();
    
    $('.sign-in-popup').on('click', '.forgot-toggler', function(event) {
        event.preventDefault();
        var $par = $(this).closest('.sign-in-popup'),
            $signin = $par.find('.sign-in-part'),
            $forgot = $par.find('.forgot-part');
            
        if ($signin.is(':visible')) {
            $signin.hide(); $forgot.show();
        }
        else {
            $forgot.hide(); $signin.show(); 
        }
    });
});

// loader plugin
(function($) {
    
    $.fn.extend({
        jbLoader: function(options) {
            this.each(function() {
                var $elem = $(this),
                    top = $elem.offset().top;
                    
                    
                $(window).on('scroll', function() {
                    if ($(this).scrollTop() > top-$(window).height()+20 && !$elem.hasClass('loaded')) {
                        $elem.addClass('loaded');
                    }
                }).trigger('scroll');
            });
            return this;
        }
    });
    
})(jQuery);

// placeholder plugin
$(function() {
    
    if ($.browser.msie && parseInt($.browser.version,10)<10) {
        $('input[type=text],input[type=password]').each(function(i,el) {
            var $input = $(el);
            if (!$input.val()) {
                if ($input.is('[type=password]')) { 
                    $input.val('********');
                    $input.focus(function(){
                        if($input.val()=='********') { $input.val(''); }
                    });
                    $input.blur(function(){
                        if($input.val()=='') { $input.val('********'); }
                    });
                }
                else {
                    var placeholder = $input.attr('placeholder');
                    if (placeholder) {
                        $input.val(placeholder);
                        $input.focus(function(){
                            if($input.val() == placeholder) { $input.val(''); }
                        });
                        $input.blur(function(){
                            if($input.val()=='') { $input.val(placeholder); }
                        });
                    }
                }
            }
        });
    }
});


(function() {
    
    // Base class for popup dialogs. Extend from this class if you want to display view in popup.
    var Overlay = function() {
        return {
            show: function() {
                if (!this.$modal) {
                    var tpl = '<div class="modal"><div class="modal-dialog"><div class="modal-popup"></div></div></div>';
                    this.$modal = $(tpl);
                    this.$modal.appendTo('body');
                    
                }
                this.$modal.show();
                
                this.$modal.children('.modal-dialog').off('click.close').on('click.close', $.proxy(function() {
                    this.hide();
                    this.$modal.trigger('onClose');
                }, this));
            },
            
            hide: function() {
                this.$modal.hide();
            }
        };
    };
    
    window.Overlay = Overlay;
    
})();