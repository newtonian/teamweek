(function() {

    var matched, browser;

    // Use of jQuery.browser is frowned upon.
    // More details: http://api.jquery.com/jQuery.browser
    // jQuery.uaMatch maintained for back-compat
    jQuery.uaMatch = function( ua ) {
	    ua = ua.toLowerCase();

	    var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
		    /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
		    /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
		    /(msie) ([\w.]+)/.exec( ua ) ||
		    ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
		    [];

	    return {
		    browser: match[ 1 ] || "",
		    version: match[ 2 ] || "0"
	    };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
	    browser[ matched.browser ] = true;
	    browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
	    browser.webkit = true;
    } else if ( browser.webkit ) {
	    browser.safari = true;
    }

    jQuery.browser = browser;

})();

jQuery(function() {
    jQuery('.accordion-toggler').click(function(e) {
        e.preventDefault();
        var jQueryaccordion = jQuery(e.target).closest('.accordion');
        jQueryaccordion.toggleClass('accordion-closed');
    });
    
    jQuery('.signin').click(jQuery.proxy(function(e) {
        e.preventDefault();
        
        if (!this.signinOverlay) { 
            this.signinOverlay = new Overlay();
        }
        this.signinOverlay.show();
        this.signinOverlay.jQuerymodal.on('onClose', jQuery.proxy(function() {
            this.signinOverlay.jQuerymodal.off('onClose');
            jQuery('.sign-in-popup').hide();
        }, this));
        
        jQuery('.sign-in-popup').show();
    }, this));
    
    
    jQuery('.first-content .text, .second-content .text').jbLoader();
    
    jQuery('.sign-in-popup').on('click', '.forgot-toggler', function(event) {
        event.preventDefault();
        var jQuerypar = jQuery(this).closest('.sign-in-popup'),
            jQuerysignin = jQuerypar.find('.sign-in-part'),
            jQueryforgot = jQuerypar.find('.forgot-part');
            
        if (jQuerysignin.is(':visible')) {
            jQuerysignin.hide(); jQueryforgot.show();
        }
        else {
            jQueryforgot.hide(); jQuerysignin.show(); 
        }
    });
});

// loader plugin
(function(jQuery) {
    
    jQuery.fn.extend({
        jbLoader: function(options) {
            this.each(function() {
                var jQueryelem = jQuery(this),
                    top = jQueryelem.offset().top;
                    
                    
                jQuery(window).on('scroll', function() {
                    if (jQuery(this).scrollTop() > top-jQuery(window).height()+20 && !jQueryelem.hasClass('loaded')) {
                        jQueryelem.addClass('loaded');
                    }
                }).trigger('scroll');
            });
            return this;
        }
    });
    
})(jQuery);

// placeholder plugin
jQuery(function() {
    
    if (jQuery.browser.msie && parseInt(jQuery.browser.version,10)<10) {
        jQuery('input[type=text],input[type=password]').each(function(i,el) {
            var jQueryinput = jQuery(el);
            if (!jQueryinput.val()) {
                if (jQueryinput.is('[type=password]')) { 
                    jQueryinput.val('********');
                    jQueryinput.focus(function(){
                        if(jQueryinput.val()=='********') { jQueryinput.val(''); }
                    });
                    jQueryinput.blur(function(){
                        if(jQueryinput.val()=='') { jQueryinput.val('********'); }
                    });
                }
                else {
                    var placeholder = jQueryinput.attr('placeholder');
                    if (placeholder) {
                        jQueryinput.val(placeholder);
                        jQueryinput.focus(function(){
                            if(jQueryinput.val() == placeholder) { jQueryinput.val(''); }
                        });
                        jQueryinput.blur(function(){
                            if(jQueryinput.val()=='') { jQueryinput.val(placeholder); }
                        });
                    }
                }
            }
        });
    }
});


(function() {
    
    // Base class for popup dialogs. Extend from this class if you want to display view in popup.
    var Overlay = function() {
        return {
            show: function() {
                if (!this.jQuerymodal) {
                    var tpl = '<div class="modal"><div class="modal-dialog"><div class="modal-popup"></div></div></div>';
                    this.jQuerymodal = jQuery(tpl);
                    this.jQuerymodal.appendTo('body');
                    
                }
                this.jQuerymodal.show();
                
                this.jQuerymodal.children('.modal-dialog').off('click.close').on('click.close', jQuery.proxy(function() {
                    this.hide();
                    this.jQuerymodal.trigger('onClose');
                }, this));
            },
            
            hide: function() {
                this.jQuerymodal.hide();
            }
        };
    };
    
    window.Overlay = Overlay;
    
})();