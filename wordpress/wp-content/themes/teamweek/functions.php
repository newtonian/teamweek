<?php
//
//  Child Theme Functions - customize your theme here,
//  all Foundation Base code kept in foundation-base.php for easy upgrades
//
include("wejnswpwhitespacefix.php");

// Short and sweet
define('WP_USE_THEMES', true);

require "foundation-base.php";

function childtheme_override_nav_above() {
    // silence
}

// add_action('thematic_belowindexbottom', 'index_bottom_news', 1);

function hildtheme_override_postfooter_postcomments() {
	//
}
function childtheme_override_postfooter_postcategory() {
	//
}

function childtheme_override_postfooter() {
	//silance
}

function krt_above_content() {

	echo '<div class="holder"><div class="post-below-content wrap-thin">';
}

add_action('thematic_abovefooter','krt_above_content', 1);

function krt_above_content_end() {
	echo '</div></div>';
}

add_action('thematic_abovefooter','krt_above_content_end', 20);

function kommentaarid() {
   		// action hook for calling the comments_template
   		if(is_single()) {
    	    thematic_comments_template();	   		
   		}
}

add_action('thematic_abovefooter','kommentaarid', 8);


//remove comments

function remove_comments(){
	if(is_page()) {
		remove_action('thematic_comments_template','thematic_include_comments',5);	
	}
}

add_action('template_redirect','remove_comments');

// Header LOGIN

function krt_login() {
	echo '<div class="sign-in-popup">
    <div class="wrap">
      <div class="sign-in-container">
        <div class="sign-in-header">Sign In</div>
        <div class="sign-in-content">
          <div class="pad-25">
            <form action="https://teamweek.com/users/sign_in
" class="sign-in-form">
              <div>
                <input type="text" class="text-input" placeholder="Your e-mail" />
              </div>
              <div class="pad-10-0-0">
                <input type="password" class="text-input" placeholder="Your password" />
              </div>
              <div class="cfx pad-15-0-0">
                <div class="float-left">
                  <label><input type="checkbox" /> Remember me</label> 
                </div>
                <div class="float-right">
                  <a href="#"><b>Forgot password?</b></a>
                </div>
              </div> <!-- //cfx -->
              <div class="pad-15-0-0 align-center">
                <input type="submit" class="btn" value="Sign In" />
              </div>
            </form>
          </div> <!-- //pad-25 -->
          <div class="gray-box align-center pad-20">
            or sign in with: &nbsp;<a href="https://teamweek.com/users/auth/google " class="btn-google"></a> <a href="https://teamweek.com/users/auth/toggl" class="btn-toggl"></a>
          </div> <!-- //gray-box -->
        </div>
      </div>
    </div>
  </div> <!-- //sign-in-popup -->	
';
}

add_action( 'thematic_header', 'krt_login', 5 );


function krt_search(){
	if(!is_single()) {
	?>
		   <div id="searchbox">	
				<form class="search" method="get" action="<?php bloginfo('home') ?>">
                    <div>
                        <input id="s" name="s" type="text" value="Search for older posts:" onfocus="if (this.value == 'Search for older posts:') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search for older posts:';}" size="32" tabindex="1">

						<input id="searchsubmit" name="searchsubmit" type="submit" value="Otsi" tabindex="2">
                    </div>
                </form>
            </div>
      	<?php	
      }		
}

add_action('thematic_abovefooter','krt_search', 8);

function triip() {
	if(is_single()) {
?>
<div class="thin-border"></div>
<?php	
	}	
}
add_action('thematic_abovefooter','triip', 5);

// misc tuning

add_filter('widget_text', 'do_shortcode');
add_post_type_support( 'page', 'excerpt' );

function remove_stuff() {
	remove_action('thematic_header', 'thematic_brandingopen', 1);
	remove_action('thematic_header', 'thematic_blogtitle', 3);
	remove_action('thematic_header', 'thematic_blogdescription', 5);
	remove_action('thematic_header', 'thematic_brandingclose', 7);
	//remove_action('thematic_header', 'thematic_access', 9);
	remove_action('thematic_footer', 'thematic_siteinfo', 30);
	remove_action('thematic_navigation_below', 'thematic_nav_below', 2);

}

add_action('init', 'remove_stuff');

// add a related articles

function childtheme_add_content_related($content) {
	$content['Content related widget area'] = array(
		'admin_menu_order' => 245,
		'args' => array (
		'name' => 'Content related widget area',
		'id' => 'content-related',
		'description' => __('Content related articles widgets', 'thematic'),
		'before_widget' => thematic_before_widget(),
		'after_widget' => thematic_after_widget(),
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	),
		'action_hook'   => 'thematic_abovefooter',
		'function'      => 'childtheme_content_related',
		'priority'      => 3
		);
	return $content;
}

add_filter('thematic_widgetized_areas', 'childtheme_add_content_related');

// set featured articles
function childtheme_content_related() {
	if(is_single()) {
		if ( is_active_sidebar('content-featured') ) {
			echo '<div class="related-posts first-related">';
			dynamic_sidebar('content-related');
			echo "</div>";
		}		
	}
}

// add a popular articles

function childtheme_add_content_featured($content) {
	$content['Content popular widget'] = array(
		'admin_menu_order' => 250,
		'args' => array (
		'name' => 'Content popular widget area',
		'id' => 'content-featured',
		'description' => __('Content popular articles widgets', 'thematic'),
		'before_widget' => thematic_before_widget(),
		'after_widget' => thematic_after_widget(),
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	),
		'action_hook'   => 'thematic_abovefooter',
		'function'      => 'childtheme_content_featured',
		'priority'      => 4
		);
	return $content;
}

add_filter('thematic_widgetized_areas', 'childtheme_add_content_featured');

// set featured articles
function childtheme_content_featured() {
	if(is_single()) {
		if ( is_active_sidebar('content-featured') ) {
			echo '<div class="related-posts second-related">';
			dynamic_sidebar('content-featured');
			echo "</div>";
		}		
	}
}

// add gray sidebar area

function childtheme_add_above_footer_widget($content) {
	$content['Above Comments Widget Area'] = array(
		'admin_menu_order' => 250,
		'args' => array (
		'name' => 'Above Comments Widget Area',
		'id' => 'above-footer-aside-widget',
		'description' => __('Above Comments Widget Area', 'thematic'),
		'before_widget' => thematic_before_widget(),
		'after_widget' => thematic_after_widget(),
		'before_title' => thematic_before_title(),
		'after_title' => thematic_after_title(),
	),
		'action_hook'   => 'thematic_abovefooter',
		'function'      => 'childtheme_above_footer_aside_widget',
		'priority'      => 21
		);
	return $content;
}

add_filter('thematic_widgetized_areas', 'childtheme_add_above_footer_widget');

// set structure for the facebook widget above the content
function childtheme_above_footer_aside_widget() {
	if ( is_active_sidebar('above-footer-aside-widget') ) {
		echo "\n".'<div class="graybox infobox">' . "\n" . "\t" . "\n";
		dynamic_sidebar('above-footer-aside-widget');
		echo "\n" . "\t" . '</div>' ."\n" . "\n";
	}
}


// add social area

function childtheme_add_social_widget($content) {
	if(is_single()) {
		$prioriteet=23;
	}
	else {$prioriteet=9;}
	$content['Social Widget Area'] = array(
		'admin_menu_order' => 250,
		'args' => array (
		'name' => 'Social Widget Area',
		'id' => 'social-widget',
		'description' => __('Social Widget Area', 'thematic'),
		'before_widget' => thematic_before_widget(),
		'after_widget' => thematic_after_widget(),
		'before_title' => thematic_before_title(),
		'after_title' => thematic_after_title(),
	),
		'action_hook'   => 'thematic_abovefooter',
		'function'      => 'childtheme_social_widget',
		'priority'      => $prioriteet
		);
	return $content;
}

add_filter('thematic_widgetized_areas', 'childtheme_add_social_widget');

// set structure for the facebook widget above the content
function childtheme_social_widget() {
	if ( is_active_sidebar('social-widget') ) {
		echo "\n".'<div class="blog-social-links">' . "\n" . "\t" . "\n";
		dynamic_sidebar('social-widget');
		echo "\n" . "\t" . '</div>' ."\n" . "\n";
	}
}

// hide unused widget areas inside the WordPress admin

function childtheme_hide_areas($content) {
    unset($content['Index Top']);
    unset($content['Index Insert']);
    unset($content['Index Bottom']);
    unset($content['Single Top']);
    unset($content['Single Insert']);
    unset($content['Single Bottom']);
    unset($content['Page Top']);
   // unset($content['Page Bottom']);
    //unset($content['Secondary Aside']);
    //unset($content['1st Subsidiary Aside']);
    unset($content['2nd Subsidiary Aside']);
    //unset($content['3rd Subsidiary Aside']);
    return $content;
}
add_filter('thematic_widgetized_areas', 'childtheme_hide_areas');

// custom scripts to wp_head

function jqueryscript_in_head(){ ?>
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/fonts.css" type="text/css" rel="stylesheet" media="all">
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/javascripts/scripts.js"></script>

	
<?php }
add_action('wp_head', 'jqueryscript_in_head');


// Begin header inner wrap open div

function start_header_inner() { ?>

<!-- footer inner wrap starts here -->

<div class="wrap">

<?php } // end of our new function start_header_inner

// Add starting div to header before branding
add_action('thematic_header','start_header_inner', 1);

// Begin header inner wrapping closing div
function end_header_inner() { ?>

</div>
<!-- header inner wrap ends here -->

<?php } // end of our new function end_header_inner

// Add closing div to header after header widget area
add_action('thematic_header','end_header_inner', 12);

// Header LOGO

function krt_logo_image() {
	echo '<a href="http://www.teamweek.com"><img id="logo" src="'.get_bloginfo( 'stylesheet_directory' ).'/images/logo.png" /></a>';
}

add_action( 'thematic_header', 'krt_logo_image', 2 );

function header_blogtitle() {
	 			echo '<div class="title"><a href="http://blog.teamweek.com"><h1>The Blog</h1></a></div>';
}

add_action( 'thematic_header', 'header_blogtitle', 10 );

// pealkirja all olev meta andmed

function childtheme_override_postmeta_entrydate() {

    $entrydate = '<span class="meta-prep meta-prep-entry-date">' . __('on', 'thematic') . ' </span>';
    $entrydate .= '<span class="entry-date"><abbr class="published" title="';
    $entrydate .= get_the_time(thematic_time_title()) . '">';
    $entrydate .= get_the_time(thematic_time_display());
    $entrydate .= '</abbr></span>';
    
    return apply_filters('childtheme_override_postmeta_entrydate', $entrydate);
   
}
	
function childtheme_override_postheader_postmeta() {
	
	$postmeta  = "\n\t\t\t\t\t";
    $postmeta .= '<div class="entry-meta">' . "\n\n";
    $postmeta .= "\t" . thematic_postmeta_authorlink() . "\n\n";

    $postmeta .= "\t" . thematic_postmeta_entrydate() . "\n\n";
    
    $postmeta .= "\t" . thematic_postmeta_editlink() . "\n\n";
    
    $postmeta .= "\t" . '<span class="meta-sep meta-sep-entry-date"> / </span>'. "\n\n";
                   
    $postmeta .= "\t" . thematic_postfooter_postcomments() . "\n\n";
    
    
    $postmeta .= '</div><!-- .entry-meta -->' . "\n";
    
    return apply_filters('thematic_postheader_postmeta',$postmeta); 

}

function childtheme_override_postmeta_authorlink() {
	global $authordata;

  //  $author_prep = '<span class="meta-prep meta-prep-author">' . __('By', 'thematic') . ' </span>';
    
    if ( thematic_is_custom_post_type() && !current_theme_supports( 'thematic_support_post_type_author_link' ) ) {
    	$author_info  = '<span class="vcard"><span class="fn nickname">';
    	$author_info .= get_the_author_meta( 'display_name' ) ;
    	$author_info .= '</span></span>';
    } else {
    	$author_info  = '<span class="author vcard">';
    	$author_info .= sprintf('<p class="url fn n" href="%s" title="%s">%s</p>',
    							get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
								/* translators: author name */
    							sprintf( esc_attr__( 'View all posts by %s', 'thematic' ), get_the_author_meta( 'display_name' ) ),
    							get_the_author_meta( 'display_name' ));
    	$author_info .= '</span>';
    }
    
    $author_credit = $author_prep . $author_info ;
    
    return apply_filters('thematic_postmeta_authorlink', $author_credit);
   
}


function index_bottom_news() {  ?>
	<div id="nav-below" class="navigation">
               		<?php if ( function_exists( 'wp_pagenavi' ) ) { ?>
                	<?php wp_pagenavi(); ?>
					<?php } else { ?>
					  
					<div class="nav-previous"><?php next_posts_link(sprintf('<span class="meta-nav">&laquo;</span> %s', __('Older posts', 'thematic') ) ) ?></div>
					
					<div class="nav-next"><?php previous_posts_link(sprintf('%s <span class="meta-nav">&raquo;</span>',__( 'Newer posts', 'thematic') ) ) ?></div>

					<?php } ?>
					
	</div>	
	<?php
}
// add_action('thematic_belowindexbottom', 'index_bottom_news', 1);

function childtheme_override_commentmeta($print = TRUE) {
	$content = '<div class="comment-meta">' . 
				sprintf( _x('%s', 'Posted {$date} at {$time}', 'thematic') , 
					get_comment_date(),
					get_comment_time() );

	$content .= ' <span class="meta-sep">|</span> ' . sprintf( '<a href="%1$s" title="%2$s">%3$s</a>', '#comment-' . get_comment_ID() , __( 'Permalink to this comment', 'thematic' ), __( 'Permalink', 'thematic' ) );
						
	if ( get_edit_comment_link() ) {
		$content .=	sprintf(' <span class="meta-sep">|</span><span class="edit-link"> <a class="comment-edit-link" href="%1$s" title="%2$s">%3$s</a></span>',
					get_edit_comment_link(),
					__( 'Edit comment' , 'thematic' ),
					__( 'Edit', 'thematic' ) );
		}
	
	$content .= '</div>' . "\n";
		
	return $print ? print(apply_filters('childtheme_override_commentmeta', $content)) : apply_filters('childtheme_override_commentmeta', $content);

} // end thematic_commentmeta

function childtheme_override_previous_post_link() {

	$args = array ( 
		'format'              => '%link',
		'link'                => '<span class="meta-nav">←</span> Older posts',
		'in_same_cat'         => FALSE,
		'excluded_categories' => ''
	);
					
	$args = apply_filters('childtheme_override_previous_post_link_args', $args );
	
	previous_post_link($args['format'], $args['link'], $args['in_same_cat'], $args['excluded_categories']);
} // end previous_post_link

function childtheme_override_next_post_link() {
	$args = array (
		'format' => '%link',
		'link' => 'Newer posts <span class="meta-nav">→</span>',
		'in_same_cat' => FALSE,
		'excluded_categories' => ''
	);
	
	$args = apply_filters('childtheme_override_next_post_link_args', $args );
	next_post_link($args['format'], $args['link'], $args['in_same_cat'], $args['excluded_categories']);
} // end next_post_link

// Begin footer inner wrap open div

function start_footer_inner() { ?>

<!-- footer inner wrap starts here -->

<div class="wrap">

<?php } // end of our new function start_header_inner

// Add starting div to footer before branding
add_action('thematic_footer','start_footer_inner', 1);

// Begin footer inner wrapping closing div
function end_footer_inner() { 
	
// Add closing div to header after header widget area
add_action('thematic_footer','end_footer_inner', 20);
?>

</div>
<!-- footer inner wrap ends here -->

<?php } // end of our new function end_header_inner

// posts archive

function posts_arcvive() {
	if(is_home()) {
	?>
	<ul class="accordion archive-posts">
		<li>
		<div class="title">
			  <h3>Posts archive</h3>
		</div>
			<div class="content">
				<ul>
			<?php
				$my_query = new WP_Query('showposts=500&offset=7');
				if( $my_query->have_posts() ) {
				while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<li><span class="date"><?php echo get_the_date('d.m.y') ?></span><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
				
				<?php
				endwhile;
				}
				wp_reset_query();
				?>
				</ul>
			</div>
		</li>
	</ul>
	<?php 
		}				
}

add_action('thematic_abovefooter','posts_arcvive', 7);

// social icons

function social_icons() {
	if(is_single()) {
		?>
		<div class="social-icons">
			<div class="s-icon">
			<?php dd_twitter_generate('Normal','teamweekplan'); ?>
			</div>
			<div class="s-icon">
			<?php dd_linkedin_generate('Normal');?>
			</div>
			<div class="s-icon">
			<?php dd_fblike_generate('Like Box Count');?>
			</div>
			<div class="s-icon">
			<?php dd_google1_generate('Normal');?>
			</div>
			<div class="s-icon">
			<?php dd_stumbleupon_generate('Normal');?>
			</div>
			<div class="s-icon">
			<?php dd_reddit_generate('Normal');?>
			</div>
		</div>
		<?php
	}
}

add_action('thematic_belowpost','social_icons', 1);

// adding author meta

function author_meta() {
	if(is_single()) {
		echo '<div class="gray-box user-box cfx">';
			echo '<div class="user-box-image">';
			the_author_image();
			echo '</div>';
				echo '<div class="user-box-content">';
					echo '<h3>';
					the_author_meta('first_name'); 
						echo ' ';
				    the_author_meta('last_name');
					echo '</h3>';
				echo '<div class="role">';
			    the_author_meta('amet');
				echo '</div>';
				the_author_meta( 'description' );
			echo '</div>';
		echo '</div>';		
	}
}
add_action('thematic_abovefooter','author_meta', 1);
?>

<?php
add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
	<h3><?php _e("Lisainfo", "blank"); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="amet"><?php _e("Minu ametikoht"); ?></label></th>
				<td>
					<input type="text" name="amet" id="amet" value="<?php echo esc_attr( get_the_author_meta( 'amet', $user->ID ) ); ?>" class="regular-text" /><br />
					<span class="description"><?php _e("Palun sisesta oma ametikoht."); ?></span>
				</td>
		</tr>
	</table>
<?php }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {

if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

update_user_meta( $user_id, 'amet', $_POST['amet'] );
}

function childtheme_override_index_loop() {
			// Count the number of posts so we can insert a widgetized area
		$count = 1;
		while ( have_posts() ) : the_post();

				// action hook for inserting content above #post
				thematic_abovepost();
				?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> > 

				<?php

	            	// creating the post header
	            	thematic_postheader();
	            ?>
     				
					<div class="entry-content">
					
						<?php thematic_content(); ?>

						<?php wp_link_pages(array('before' => sprintf('<div class="page-link">%s', __('Pages:', 'thematic')),
													'after' => '</div>')); ?>
					
					</div><!-- .entry-content -->
					
					<?php thematic_postfooter(); ?>
				
				</div><!-- #post -->

			<?php 
				// action hook for insterting content below #post
				thematic_belowpost();
				
				comments_template();

				if ( $count == thematic_get_theme_opt( 'index_insert' ) ) {
					get_sidebar('index-insert');
				}
				$count = $count + 1;
		endwhile;
}

function alter_comment_form_fields($fields){
    $fields['author'] = '<p class="comment-form-author">' . ( $req ? '<span class="required">*</span>' : '' ) .
                    '<input id="author" name="author" type="text" placeholder="Name (required)" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>';
    $fields['email'] = '';  //removes email field
    $fields['url'] = '';  //removes website field

    return $fields;
}
add_filter('comment_form_default_fields','alter_comment_form_fields');

function childtheme_override_postfooter_postcomments() {
	if (comments_open()) {
	$postcommentnumber = get_comments_number();
	
	if ($postcommentnumber > '0') {
		$postcomments = sprintf('<span class="comments-link"><a href="%s" title="%s" rel="bookmark">%s</a></span>',
							apply_filters('the_permalink', get_permalink()) . '#comments',
							sprintf( esc_attr__('Comment on %s', 'thematic'), the_title_attribute( 'echo=0' ) ),
							/* translators: number of comments and trackbacks */
							sprintf( _n('%s Response', '%s Responses', $postcommentnumber, 'thematic'), number_format_i18n( $postcommentnumber ) ) );
	} else {
	    $postcomments = sprintf('<span class="comments-link"><a href="%s" title="%s" rel="bookmark">%s</a></span>',
							apply_filters('the_permalink', get_permalink()) . '#comments',
							sprintf( esc_attr__('Comment on %s', 'thematic'), the_title_attribute( 'echo=0' ) ),
							__('Leave a comment', 'thematic'));
	}
	} else {
	$postcomments = '<span class="comments-link comments-closed-link">' . __('Comments closed', 'thematic') .'</span>';
	}            
	return apply_filters('thematic_postfooter_postcomments',$postcomments); 
}
?>