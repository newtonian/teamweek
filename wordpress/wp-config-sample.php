<?php

define( 'PROD_HOST', '' );
define( 'CURRENT_HOST', $_SERVER['SERVER_NAME'] );
define( 'CURRENT_IP', $_SERVER['SERVER_ADDR'] );

define( 'WP_DEBUG', false ) ;
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );
$table_prefix  = 'wp_';
define( 'WPLANG', 'et' );
define( 'WP_MEMORY_LIMIT', '96M' ); // 96M needed for WPML
define( 'UPLOADS', 'static' );

if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {

	include( dirname( __FILE__ ) . '/local-config.php' );
  
} else {

	if ( CURRENT_IP == "127.0.0.1" || CURRENT_IP == "10.10.10.2" ) {
	// 10.10.10.2 to be used as localhost IP for browser testing from VMs 
		
		define('WP_LOCAL_DEV', true);
		
		// for MAMP local dev to avoid admin css/js concatenation problems - http://stackoverflow.com/q/12413305/2000872
		define('CONCATENATE_SCRIPTS', false );
	
	// local test server conf goes here
	
define('DB_NAME', 'database_name_here');
define('DB_USER', 'username_here');
define('DB_PASSWORD', 'password_here');
define('DB_HOST', 'localhost');
		
	// make site run on localhost, .local etc
		
		define('WP_SITEURL', 'http://' . CURRENT_HOST );
		define('WP_HOME',    'http://' . CURRENT_HOST );
		
	// replace URLs in content that might be hard-coded to production (placed images etc)
		
		if ( CURRENT_HOST != PROD_HOST ) {
			ob_start( function ( $page ) {
				return str_replace( PROD_HOST, CURRENT_HOST, $page ); 
			} );
		}
		
	} else {
	
		define('WP_LOCAL_DEV', false);
	
	// production server conf goes here OR into local-config.php
		
		define('DB_NAME', '');
		define('DB_USER', '');
		define('DB_PASSWORD', '');
		define('DB_HOST', 'dxxxxx.mysql.zone.ee');

		define('WP_SITEURL', 'http://' . CURRENT_HOST );
		define('WP_HOME',    'http://' . CURRENT_HOST );
		
	// do not allow WP/plugins update on production

		define('DISALLOW_FILE_MODS',true);
	}

}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');

/**#@-*/

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
